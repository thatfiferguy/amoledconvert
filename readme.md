# AMOLED convert
## Purpose
`amoledconvert.py` was created to optimize images intended to be backgrounds for AMOLED displays. AMOLED displays are able to save power by only powering pixels which are not true black (0,0,0 in rgb). However, many dark backgrounds which appear suitable for AMOLED displays don't actually feature many true black pixels but instead have pixels which appear black to the human eye but in reality just have low rgb values.

`amoledconvert.py` takes an image and converts these close to black pixels to true black. It also displays statistics about how many pixels (count and percentage) are true black before and after the conversion. The threshold of what is "close to black" is given by an argument -- the higher the number, the more pixels will be converted but the worse dark colors in the image will be affected.

## Usage
`python amoledconvert.py file threshold`

* **file**: the path to the image to convert.
* **threshold**: a number -- the rgb value threshold under which dark colors are converted to black.

## Example
![before conversion](example/render.png "before conversion")
Before conversion



---

![threshold 10](example/outfile10.png "threshold 10")
Threshold 10: All pixels with R,G,B values all below 10 are set to true black.
```text
$ python amoledconvert.py render.png 10
saved to outfile.png. Of 2073600 total pixels:
	7819 (0.38%) pixels were true black before conversion
	506870 (24.44%) pixels are true black after conversion
```
---

![threshold 20](example/outfile20.png "threshold 20")
Threshold 20: All pixels with R,G,B values all below 20 are set to true black.
```text
$ python amoledconvert.py render.png 20
saved to outfile.png. Of 2073600 total pixels:
	7819 (0.38%) pixels were true black before conversion
	1412543 (68.12%) pixels are true black after conversion
```

---
![threshold 50](example/outfile50.png "threshold 50")
Threshold 50: All pixels with R,G,B values all below 50 are set to true black.

```text
$ python amoledconvert.py render.png 50
saved to outfile.png. Of 2073600 total pixels:
	7819 (0.38%) pixels were true black before conversion
	1922266 (92.7%) pixels are true black after conversion
```
