import sys
from PIL import Image

def usage():
    print "usage: python amoledconvert.py file threshold"
    print "\t file: the name of the file to convert."
    print "\t threshold: pixels with r,g,b values below this number will be converted to true black. The higher the number, the more pixels will be converted."
    exit()

# help
if len(sys.argv) < 3:
    usage()


# argument checking
try:
    float(sys.argv[2])
except ValueError:
    print "ERROR: threshold is not a number"
    usage()

# file stuff
try:
    img = Image.open(sys.argv[1]);
except IOError:
    print "ERROR: file does not exist or is of wrong type"
    usage()

imgcopy = list(img.getdata())

# initialize counts
blackcount = 0
alreadyblackcount = 0
pixelcount = 0

# iterate through all pixels
for i in range(0,len(imgcopy)):
    pixelcount +=1
    pixel = imgcopy[i]

    # check if pixel is already true black
    if pixel[0] == 0 and pixel[1] == 0 and pixel[2] == 0:
        alreadyblackcount += 1
        blackcount += 1
    # else check if it is within the proper range to be converted to true black
    elif pixel[0] < float(sys.argv[2]) and pixel[1] < float(sys.argv[2]) and pixel[2] < float(sys.argv[2]):
        imgcopy[i] = (0,0,0,255)
        # print "\tconverted pixel"
        blackcount += 1

# save to new file
img.putdata(imgcopy)
img.save("outfile.png")

# print stats
print "saved to outfile.png. Of", pixelcount, "total pixels:"

oldpercentage = alreadyblackcount/float(pixelcount) * 100
print "\t", alreadyblackcount, "(" + str(round(oldpercentage,2)) + "%) pixels were true black before conversion"

newpercentage = blackcount/float(pixelcount) * 100
print "\t", blackcount, "(" + str(round(newpercentage,2)) + "%) pixels are true black after conversion"
